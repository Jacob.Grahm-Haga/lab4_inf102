package INF102.lab4.median;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class QuickMedian implements IMedian {

    @Override
    public <T extends Comparable<T>> T median(List<T> originalList) { //Kickstart quicksort
        List<T> list = new ArrayList<>(originalList); // Method should not alter list
        int targetI = list.size()/2;
        return(quickSort(list, targetI));
    }

    public <T extends Comparable<T>> T quickSort(List<T> list, int targetI){
        if (list.size() == 1){ //Break recursion on 1 element left
            return(list.get(0));
        }

        Random r = new Random();
        T pivot = list.get(r.nextInt(list.size()));

        List<T> low = new ArrayList<>();
        List<T> high = new ArrayList<>();
        List<T> pivots = new ArrayList<>();

        for (T item : list) {
            if (item.compareTo(pivot) == 0){ //If item is equal to pivot
                pivots.add(item);
            } else if (item.compareTo(pivot) < 0){ //If item is greater than pivot
                high.add(item);
            } else { //If item is smaller or equal to pivot
                low.add(item);
            }
        }
        if (targetI < low.size()){
            return quickSort(low, targetI);
        } else if (targetI < low.size() + pivots.size()){
            return pivots.get(0);
        } else {
            return quickSort(high, targetI - low.size() - pivots.size());
        }
    }

}
