package INF102.lab4.sorting;

import java.util.List;

public class BubbleSort implements ISort {

    @Override
    public <T extends Comparable<T>> void sort(List<T> list) {
        Boolean active = true;
        while (active){
            active = false; //Base case: loop terminates

            for (int i = 1; i < list.size(); i++) {
                T current = list.get(i);
                T prev = list.get(i-1);
                
                if (current.compareTo(prev) < 0){//if current is smaller than prev
                    //Swap elements
                    list.set(i, prev);
                    list.set(i-1, current);

                    //repeat loop if swapped
                    active = true;
                }
            }
        }
    }
    
}
